<?php

namespace backend\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;



/**
 * This is the model class for table "user".
 *
 * @property int $id
 * @property string $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $email
 * @property string $user_type
 * @property int $role_id
 * @property int $status
 * @property string $created_at
 * @property string $updated_at
 * @property string $verification_token
 */
class User extends \yii\db\ActiveRecord
{

    public $confirm_password;
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user';
    }

    public function behaviors()
    {
       
        return [

            'timestamp' => [

                'class' => 'yii\behaviors\TimestampBehavior',

                'attributes' => [

                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],

                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],

                ],

                'value' => new Expression('NOW()'),

            ],           

        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['first_name','last_name', 'email', 'role_id', 'status','password_hash','confirm_password'], 'required','on' => 'create'],
            [['email'], 'unique','targetClass' => User::className(), 'message' => 'This email address is already exists.'],
            ['confirm_password', 'compare', 'compareAttribute' => 'password_hash' ,'on' => 'create'],
            [['first_name','last_name', 'email', 'role_id', 'status'], 'required','on' => 'update'],
            [['role_id'], 'integer'],
            [['created_at', 'auth_key', 'password_hash', 'updated_at','username'], 'safe'],
            [['username', 'password_hash', 'password_reset_token', 'email', 'verification_token'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['username'], 'unique'],
            [['email'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'role_id' => 'Role',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
        ];
    }

    public function getuserrole()
    {
        return $this->hasOne(Roles::className(), ['id' => 'role_id']);
    }
}
