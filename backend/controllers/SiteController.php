<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use common\models\User;
use yii\helpers\Url;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error','forgotpassword','resetpassword'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    public function actionForgotpassword()
    { 
        $model = new LoginForm();
        $model->scenario = 'forgotpassword';
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = \yii\web\Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post()))
        {
                $userDetails = User::find()->select(['id','first_name','last_name','email'])->where(['email' => $_POST['LoginForm']['email']])->one();
                if(!empty($userDetails))
                {
                     $userDetails->generatePasswordResetToken();
                    $userDetails->save();
                    
                    $link = Url::to(['site/resetpassword', 'id' => $userDetails->password_reset_token],true);
                    
                $mailData = array('first_name' => $userDetails->first_name,'link' => $link);
               $body    = $this->renderPartial('../mail_templates/forgot_password',['data' => $mailData]);
                   $from_email = Yii::$app->params['defult_from_email'];              
                    $emailSubject = 'Forgot Password Request - '.Yii::$app->name;
                  $send=   Yii::$app->mailer->compose()
                        ->setTo($userDetails->email)
                        ->setFrom($from_email)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($body)
                        ->send();
                  // print_r($send);die;

                    Yii::$app->session->setFlash('successmessage','Please check your mail.');
                    return $this->goBack();
                }else{
                    Yii::$app->session->setFlash('failedmessage','Sorry! Email address not exists.');
                    return $this->refresh();
                }
           
        } else {

            return $this->render('forgotpassword', [
                'model' => $model,
            ]);
        }
    }

    public function actionResetpassword($id)
    {
         $Usermodel = User::findByPasswordResetToken($id);
       //  print_r($Usermodel);die;
        if(empty($Usermodel))
        {
            Yii::$app->session->setFlash('failedmessage','User does not exists.');
            return $this->goBack();
        }

        $model = new LoginForm();
        $model->scenario = 'resetpassword';

        if ($model->load(Yii::$app->request->post()))
        {

            $Usermodel->setPassword($_POST['LoginForm']['password']); 
            $Usermodel->removePasswordResetToken();
            if($Usermodel->save()){
             
                $mailData = array('first_name' => $Usermodel->first_name);
                $body    = $this->renderPartial('../mail_templates/reset_password',['data' => $mailData]);
                $emailSubject = 'Password Reset - '.Yii::$app->name;
                $from_email = Yii::$app->params['defult_from_email'];              
                Yii::$app->mailer->compose()
                    ->setTo($Usermodel->email)
                    ->setFrom($from_email)
                    ->setSubject($emailSubject)
                    ->setHtmlBody($body)
                    ->send();

                Yii::$app->session->setFlash('successmessage','Your password has been changed successfully');
            }else{
                Yii::$app->session->setFlash('failedmessage','Please try again. Something went wrong.');
            }
            return $this->goBack();

        } else {
            return $this->render('resetpassword', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }
}
