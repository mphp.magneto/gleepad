<?php

namespace backend\controllers;

use Yii;
use backend\models\User;
use backend\models\UserSearch;
use backend\models\Roles;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\widgets\ActiveForm;
use yii\web\Response;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new UserSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        $model->scenario= 'create';
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
                        Yii::$app->response->format = Response::FORMAT_JSON;
                        return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {               
            $model->password_hash = $model->confirm_password = Yii::$app->security->generatePasswordHash($model->password_hash);
            if($model->save()){
                    $userDetails = User::find()->select(['id','first_name','last_name','email'])->where(['email' => $_POST['User']['email']])->one();
                    if(!empty($userDetails))
                    {
                        /*$userDetails->generatePasswordResetToken();
                        $userDetails->save();
                    
                        $link = Url::to(['site/resetpassword', 'id' => $userDetails->password_reset_token],true);*/
                    
                        $mailData = array('first_name' => $userDetails->first_name,'last_name' => $userDetails->last_name,'email'=>$userDetails->email,'password'=>$_POST['User']['password']);
                        $body    = $this->renderPartial('../mail_templates/forgot_password',['data' => $mailData]);
                        $from_email = Yii::$app->params['defult_from_email'];              
                        $emailSubject = 'Forgot Password Request - '.Yii::$app->name;
                        $send=   Yii::$app->mailer->compose()
                        ->setTo($userDetails->email)
                        ->setFrom($from_email)
                        ->setSubject($emailSubject)
                        ->setHtmlBody($body)
                        ->send();
                    }
                  // print_r($send);die;
                Yii::$app->session->setFlash('successmessage', 'User has been created successfully.');
            } else {
                Yii::$app->session->setFlash('failedmessage', 'Failed to create the User.');
            }
            return $this->redirect(['index']);
        }
        $rolesList = Roles::getRoles();
        return $this->render('create', [
            'model' => $model,
            'rolesList' => $rolesList,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $model->scenario= 'update';
        
        $rolesList = Roles::getRoles();

        if ($model->load(Yii::$app->request->post()) ) {
            if($model->save()){
                Yii::$app->session->setFlash('successmessage', 'User has been updated successfully.');
            } else {
                pre($model->getErrors());
                Yii::$app->session->setFlash('failedmessage', 'Failed to updated the User.');
            }
            return $this->redirect(['index']);
        }

        return $this->render('update', [
            'model' => $model,
            'rolesList' => $rolesList,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
