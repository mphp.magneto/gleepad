<aside class="main-sidebar">

    <section class="sidebar">


        <?= dmstr\widgets\Menu::widget(
            [
                'options' => ['class' => 'sidebar-menu tree', 'data-widget'=> 'tree'],
                'items' => [
                    
                    [
                        'label' => 'Users',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create User', 'icon' => 'user-plus', 'url' => ['/user/create']],
                            ['label' => 'Manage Users', 'icon' => 'users', 'url' => ['/user']],
                        ],
                    ],
                    [
                        'label' => 'Products',
                        'icon' => 'users',
                        'url' => '#',
                        'items' => [
                            ['label' => 'Create Product', 'icon' => 'user-plus', 'url' => ['/product/create']],
                            ['label' => 'Manage Products', 'icon' => 'users', 'url' => ['/product']],
                            /*['label' => 'Create Attribute', 'icon' => 'user-plus', 'url' => ['/attribute/create']],
                            ['label' => 'Manage Attributes', 'icon' => 'users', 'url' => ['/attribute']],*/
                            ['label' => 'Create Product Category', 'icon' => 'user-plus', 'url' => ['/product-category/create']],
                            ['label' => 'Manage Product Category', 'icon' => 'users', 'url' => ['/product-category']],
                        ],
                    ],
                ],
            ]
        ) ?>

    </section>

</aside>
