<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Forgot Password';


?>

<div class="login-box">
     <?php if(Yii::$app->session->getFlash('successmessage')) { echo  '<div class="alert-success" id="successmessage">'.Yii::$app->session->getFlash('successmessage').'</div>'; } ?>
    <?php if(Yii::$app->session->getFlash('failedmessage')) { echo  '<div class="alert-danger" id="failedmessage">'.Yii::$app->session->getFlash('failedmessage').'</div>'; } ?>
   
   
    <!-- /.login-logo -->
    <div class="login-box-body">
     <div class="login-logo">
        <a href="#"><b><?=Yii::$app->name;?></b></a>
    </div>

        <?php $form = ActiveForm::begin(['id' => 'login-form', 'enableClientValidation' => true, 'enableAjaxValidation' => true]); ?>
       
       <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>
       

        

        <div class="row">
            <div class="col-xs-4">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
            <div class="col-xs-6">
                    <?= Html::a(
                        'Back to Login',
                        ['/site/login'],
                        ['class'=> 'btn btn-default btn-flat']
                    ) ?>
                </div>
            </div>


        <?php ActiveForm::end(); ?>

        
        
    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
