<?php
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

$this->title = 'Reset Password';

?>

<div class="login-box">
    
    <!-- /.login-logo -->
    <div class="login-box-body">
        
       <div class="login-logo">
        <a href="#"><b><?=Yii::$app->name;?></b></a>
    </div>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>

        <?= $form->field($model, 'password')->passwordInput(['maxlength' => 20]) ?>

        <?= $form->field($model, 'confirmPassword')->passwordInput(['maxlength' => 20])->label("Confirm Password"); ?>

        <div class="row">
            
            <!-- /.col -->
            <div class="col-xs-4">
                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-block btn-flat', 'name' => 'login-button']) ?>
            </div>
             <div class="col-xs-6">
                    <?= Html::a(
                        'Back to Login',
                        ['/site/login'],
                        ['class'=> 'btn btn-default btn-flat']
                    ) ?>
                </div>
            <!-- /.col -->
        </div>


        <?php ActiveForm::end(); ?>

       

      
        

    </div>
    <!-- /.login-box-body -->
</div><!-- /.login-box -->
