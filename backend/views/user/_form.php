<?php

use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">

    <?php $form = ActiveForm::begin(['id'=>'add-user','enableClientValidation' => false, 'enableAjaxValidation' => true],['enctype'=>'multipart/form-data']); ?>
<div class="form-group row">
    <div class = "col-md-6">
    <?= $form->field($model, 'first_name')->textInput(['maxlength' => true]) ?>
    </div>
      <div class = "col-md-6">
    <?= $form->field($model, 'last_name')->textInput(['maxlength' => true]) ?>
    </div>
</div>

<div class="form-group row">
    <div class = "col-md-6">
    <?= $form->field($model, 'email')->textInput(['maxlength' => true]) ?>
    </div>
    <div class = "col-md-6">
    <?= $form->field($model, 'role_id')->dropDownList(ArrayHelper::map($rolesList, 'id', 'name'),['prompt' => SELECT , 'class'=>'form-control selectpicker']) ?>
    </div>
</div>
<?php if($model->isNewRecord) { ?>
<div class="form-group row">
    <div class = "col-md-6">
    <?= $form->field($model, 'password_hash')->textInput(); ?>
    </div>
    <div class = "col-md-6">
    <?= $form->field($model, 'confirm_password')->textInput(); ?>
    </div>
</div>
<?php } ?>
<div class="form-group row">
    <div class = "col-md-6">
    <?= $form->field($model, 'status')->dropDownList(Yii::$app->params['user_status'],['prompt' => SELECT , 'class'=>'form-control selectpicker']) ?>
        
    </div>
</div>


    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        <?php  echo Html::a('Cancel',['index'],['class'=>'btn btn-danger']);  ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
