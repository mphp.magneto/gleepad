<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel backend\models\UserSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-index">
    <?php
    if (Yii::$app->session->getFlash('successmessage')) {
        echo '<div class="alert alert-success" >' . Yii::$app->session->getFlash('successmessage') . '</div>';
    }
    ?>
    <?php
    if (Yii::$app->session->getFlash('failedmessage')) {
        echo '<div class="alert alert-danger" >' . Yii::$app->session->getFlash('failedmessage') . '</div>';
    }
    ?>


    <p style="float:right;">
        <?= Html::a('Create User', ['create'], ['class' => 'btn btn-success']) ?>
        <?php  echo Html::a('Delete', ['#'], ['class' => 'btn btn-danger','id'=>'delbtn']) ; ?>
    </p>

    <?php Pjax::begin(['id' => 'users-pjax-container']); ?>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'options' => ['class' => 'grid-view table-responsive', 'style' => 'cursor:pointer'],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'class' => 'yii\grid\CheckboxColumn',
                'checkboxOptions' => function($model, $key, $index, $column) {
                          return ['value' => $model->id];
                }
            ],

            // 'id',
            // 'username',
            'first_name',
            'last_name',
            // 'auth_key',
            // 'password_hash',
            // 'password_reset_token',
            'email:email',
            [
                'attribute'=>'role_id',
                'value'=>'userrole.name'
            ],
            [
                'attribute'=>'status',
                'filter'=>Html::activeDropDownList($searchModel,'status',["1"=>"Active","2"=>"Inactive"],['class'=>'form-control','prompt'=>'All']),
                'content'=>function($data){
                    if($data->status == '1')
                        return 'Active';
                    else
                        return 'In Active';
                }
            ],
            'created_at',

            ['class' => 'yii\grid\ActionColumn','template'=>"{update} {delete}"],
        ],
    ]); ?>

    <?php Pjax::end(); ?>

</div>
<style type="text/css">
    #users-pjax-container
    {
        clear: both;
    }
</style>