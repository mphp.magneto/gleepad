<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'defult_from_email'=>'testmail.magneto@gmail.com',
    'user_status'=>['1'=>'Active','2'=>'Inactive']
    // 'user_status'=>['0'=>'Deleted','1'=>'Active','2'=>'Inactive']
];
