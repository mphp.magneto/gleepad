<?php
return [
    'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=27.109.19.234;dbname=gleepad',
            'username' => 'gleepad',
            'password' => 'gleepad', 
            'charset' => 'utf8',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            //'viewPath' => '@common/mail',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => false,
            'transport' => [
                'class' => 'Swift_SmtpTransport',
                'host' => 'smtp.gmail.com',
                'username' => 'testmail.magneto@gmail.com',
                'password' => 'Magneto@999',
                'port' => '465',
                'encryption' => 'ssl',
                ],
        ],
    ],
];
