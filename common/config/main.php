<?php
return [
    'aliases' => [
        '@bower' => '@vendor/bower-asset',
        '@npm'   => '@vendor/npm-asset',
    ],
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
    ],
     'timeZone'=>'America/New_York',
    'components' => [
        'cache' => [
            'class' => 'yii\caching\FileCache',

        ],
        'authManager' => [
        'class' => 'yii\rbac\DbManager', // or use 'yii\rbac\PhpManager'
         ],
         'user' => [
       // 'class' => 'mdm\admin\models\User',
        'identityClass' => 'mdm\admin\models\User',
       // 'loginUrl' => ['admin/user/login'],
         ],
         
 
    ],
    'as beforeRequest' => [  //if guest user access site so, redirect to login page.
        'class' => 'yii\filters\AccessControl',
        'rules' => [
            [
                'actions' => ['login','forgotpassword','resetpassword'],
                'allow' => true,
            ],
            [
                'allow' => true,
                'roles' => ['@'],
            ],
        ],
    ],
];
